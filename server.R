require(shiny)
require(visNetwork)
library(shinyjs)
library(igraph)


shinyServer(function(input, output) {
  
  source("./src/shiny/server/funnmg_server.R", local = TRUE, encoding = "UTF-8")
  source("./src/shiny/server/sample_server.R", local = TRUE, encoding = "UTF-8")
  source("./src/shiny/server/upload_server.R", local = TRUE, encoding = "UTF-8")
  source("./src/shiny/server/network_server.R", local = TRUE, encoding = "UTF-8")
  source("./src/shiny/server/plot_data_server.R", local = TRUE, encoding = "UTF-8")
  
})
