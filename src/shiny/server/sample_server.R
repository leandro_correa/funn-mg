INPUT_PATH <- "./input/ocean_sample/"

printInShinyBox <- function(phrase){
  foo <- function(message_teste) {
    message(message_teste)
    Sys.sleep(0.5)
  }
  
  withCallingHandlers({
    shinyjs::html("text", "")
    foo(phrase)
  },
  message = function(m) {
    shinyjs::html(id = "text", html = m$message, add = TRUE)
  })
}

observeEvent(input$submit, {
    #printInShinyBox()
    tab.input <- read.csv(file = paste0(INPUT_PATH,input$sample,".csv"))
  
    output$dataTable <- DT::renderDataTable(
      
      tab.input, options = list(lengthChange = FALSE,
                                pageLength = 20)
    )
})

