#######################################  
#
# FUNN-MG: FuNctioNal Network Analysis of MetaGenomics Data
#
# Author: Leandro Correa
# Version: Thu 7 September 14:41:00 2016
# 
# input  : g1, rdp, gp_df, table_information.csv
# output : table_metrics_mgpm, g1, rdp, gp_df, table_informtation.csv
########################################
source(paste0(getwd(),"/src/analysis/bibfunction.R"))
rm(list=setdiff(ls(), c("neighborhoodConnectivity")))
load(paste0(getwd(),"/data/local_database/localVar.RData"))
load(paste0(OUTPUT_PATH,"/data/PathwayNetwork.RData"))

## PT[Br] Extraindo medidas de grau de conectividade (gene-pathway Network)
## Extracting degree of connectivity (gene-pathway Network)
indegree_g1 <- degree(g1,v=V(g1),mode="in")
indegree_g1_deg_dist <- degree.distribution(g1,cumulative=T,mode="in")
indegree_g1_pwl <- power.law.fit(indegree_g1)

## PT[Br] Plotando o grau de distribução (gene-pahway Network)
## Plotting in degree distribution (gene-pathway Network)
jpeg(paste0(OUTPUT_PATH ,"/images/MGPN_powerLaw.jpg"))
plot(indegree_g1_deg_dist,
     log="xy",
     ylim=c(.01,10),
     bg="#181818",pch=21,
     xlab="Degree",
     ylab="Cumulative Frequency",
     main = "Degree distribution of Microbial Gene Pathway Network")

# and the expected power law distribution
lines(1:200,10*(1:200)^((-indegree_g1_pwl$alpha)+1), col = "red")
dev.off()

## PT[Br] Extraindo medida de centralidade de intermediação (gene-pathway Network)
## Extracting betweenness centrality (gene-pathway Network)
bet_centrality_g1 <- betweenness(g1, v=V(g1), directed = FALSE, 
                                 nobigint = TRUE, normalized = FALSE)

## PT[Br] Plotando medida de centralidade de intermediação (gene-pathway Network, só pathways)
## Plotting betweennes cetrality distribution (gene-pathway Network, only pathways)

jpeg(paste0(OUTPUT_PATH ,"/images/MGPN_betwennes_centrality.jpg"))
plot(bet_centrality_g1[which(gp_df[,"nameOrg"] == "Pathway")], 
     xlab="Pathways",
     ylab="Number of intermediations",
     pch = 16,
     col = "#181818",
     main = "Betwennes centrality extracted of MGPN")
#axis(1, at= 0:(length(namesp)-1), labels=namesp, las=2, pos=-5.0)
dev.off()

## PT[Br] Extraindo media de conectividade entre os vizinhos de cada nodo (gene-pathway Network)
## Extract neighborhood connectivity (gene-pathway Network)
neighborhoodConnectivity_g1 <- neighborhoodConnectivity(g1)

## PT[Br] Plotando media de conectividade entre os vizinhos (gene-pathway Network)
## Plotting neighborhood connectivity distribution (gene-pathway Network)
jpeg(paste0(OUTPUT_PATH ,"/images/MGPN_neighborhood_Connectivity.jpg"))
plot(neighborhoodConnectivity_g1[which(gp_df[,"nameOrg"] == "Pathway")], 
     xlab="Pathways",
     ylab="Average connectivity between neighbors",
     pch = 16,
     col = "#181818",
     main = "Neighborhood Connectivity MGPN")
dev.off()

## PT[Br] Craindo uma tabela com os resultados das métricas extraídas da rede MGPN
## Creating a table with the results of the metrics extracted from MGPN network
table_metrics_mgpn <- cbind(bet_centrality_g1, neighborhoodConnectivity_g1)
colummNames <- c("betweenness_centrality","neighborhood_connectivity")

colnames(table_metrics_mgpn) <- colummNames; rownames(table_metrics_mgpn) <- as.character(gp_df$name)

## PT[Br] Escrevendo as tabelas em arquivo .csv
## Writing tables in .csv file
#write.csv(file = paste0(OUTPUT_PATH ,"/tables/MGPN_network_metrics_of_genes.csv"),
#          table_metrics_mgpn[which(!(gp_df$nameOrg == "Pathway")),])
table_informtation <- read.csv(file = paste0(OUTPUT_PATH ,"/tables/Pathways_information.csv"))
colname <- c(colnames(table_informtation),colummNames)
table_informtation <- cbind(table_informtation,table_metrics_mgpn[which((gp_df$nameOrg == "Pathway")),])
colnames(table_informtation) <- colname
write.csv(file = paste0(OUTPUT_PATH ,"/tables/Pathways_information.csv"),table_informtation, row.names = FALSE)

## PT[Br] Removendo variáveis desnecessárias
## Removing unnecessary variables
LOCAL_VAR <- c("table_metrics_mgpn","gp_df","g1","rdp","OUTPUT_PATH")
rm(list=setdiff(ls(), LOCAL_VAR))
## PT[Br] Salvando os dados
## Saving data
save.image(paste0(OUTPUT_PATH,"/data/NetworkMetrics.RData"))

