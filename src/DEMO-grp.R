#######################################	
#
# FUNN-MG: FuNctioNal Network Analysis of MetaGenomics Data
#
# Author: Leandro Correa
# Version: Thu 7 April 14:46:00 2016
# 
# input : funn-mg/input 
# output : Microbial Pathway Network  
#################################################################################################

#INPUT_PATH = "~/R/funn-mg/input/ocean_sample/T30800.csv"

#################################################################################################
DATASET = strsplit(INPUT_PATH,"/")
DATASET = DATASET[[1]][length(DATASET[[1]])]
DATASET = gsub('[.csv].*', "\\1", DATASET)
ANALYSIS = "GROUPS"
LOCAL_KEGG_PATH = paste0(getwd(),"/data/local_database/Procarioto_Table.csv")
OUTPUT_PATH = paste0(getwd(),"/output/", DATASET)
dir.create(OUTPUT_PATH, showWarnings = FALSE)
OUTPUT_PATH = paste0(OUTPUT_PATH,"/groups")
dir.create(OUTPUT_PATH, showWarnings = FALSE)
save.image(paste0(getwd(),"/data/local_database/localVar.RData"))
#################################################################################################

### Conection with KEGG and organization of information 
source(paste0(getwd(),"/src/groups/orthology_structure.R"))
## Functional enrichment analysis of metabolic pathways from the groups
source(paste0(getwd(),"/src/groups/orthology_enrichment.R"))


## Visual representation of the network gene pathways
source(paste0(getwd(),"/src/analysis/pathway_Network.R"))
## Extract important metrics of the above identified groups 
source(paste0(getwd(),"/src/analysis/network_Metrics.R"))
## Resilience test set of metabolic pathways
source(paste0(getwd(),"/src/analysis/network_Analysis.R"))


mywait()
## Visual representation of the FUNN-MG network about groups analysis
source(paste0(getwd(),"/src/groups/funn_Analysis.R"))
